import javax.swing.JFrame;

public class Main_Smithing extends JFrame {

		public static void main(String[] args) {
			Equipment equip = new Equipment();
			Gui gui = new Gui("BlackSmith");

			equip.setMaterial(equip.materials.get(equip.selectIngot()));

			equip.setWeaponName(equip.weapons.get(equip.selectWeapon()));

			gui.setEventStart(equip.smithingWork(equip.getMaterial(), equip.getWeaponName()));

	}

}
