import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**親クラス「Ingot」 素材の名前
 * @author 200217AM*/
public abstract class Ingot {
	//=================フィールド値===================

	/**[material]最終的に出力する*/
	private String material;

	/**[random]言わずもがな、乱数発生装置*/
	Random random = new Random();

	/**「material」の格納元ArrayList
	 *コンストラクタで初期設定:追加可能*/
	List<String> materials = new ArrayList<>();

	//================以下、メソッド==================

	/**private[material]出力用
	 * @return material*/
	public String getMaterial() {
		return material;
	}

	/*private[material]入力用
	 * (.materials.get(.selectIngot())*/
	public void setMaterial(String materialName) {
		this.material = materialName;
	}

	/**[materials](ArrayList)の数をランダムなint値で返す
	 * @return int select*/
	public int selectIngot(){
	int select = random.nextInt(materials.size());
	return select;
	}

	/**コンストラクタ（引数なし)
	 *「materials」に突っ込む初期設定群
	 *必要であれば以下.addで追加*/
	public Ingot(){
	materials.add("アイアン");
	materials.add("カッパー");
	materials.add("ブラス");
	materials.add("スチール");
	materials.add("チタン");
	materials.add("モリブテン");
	materials.add("シルバー");
	materials.add("ステンレス");
	materials.add("なまもの");
	}
}
