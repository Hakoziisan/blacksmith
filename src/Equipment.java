import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**[Equipment]装備品の名前・製造メソッド
 * @author 200217AM*/
public class Equipment extends Ingot implements Smith{
	//===================フィールド値===================

	/**[weaponName]最終的に出力する */
	private String weaponName;

	/**[scanner]文字入力用 */
	Scanner scanner = new Scanner(System.in);

	/**[random]言わずもがな、乱数発生装置
	 * ([selectWeapon]で使用予定）*/
	Random random = new Random();

	/**[weaponName]の格納元ArrayList
	 * コンストラクタで初期設定：追加可能*/
	List<String> weapons = new ArrayList<>();

//==================以下、メソッド==================

	/*[weaponName]出力用（getter*/
	public String getWeaponName() {
		return weaponName;
	}

	/*[weaponName]入力用(setter
	 *(.weapons.get(.selectWeapon())*/
	public void setWeaponName(String weaponName) {
		this.weaponName = weaponName;
	}

	/**[weapons](ArrayList)の数をランダムなint値で返す
	 * @return　int select*/
	public int selectWeapon(){
	int select = random.nextInt(weapons.size());
	return select;
	}

	/**[order]注文メソッド（トリガー用　（main未実装）
	 * switch構文・構成（case:１, case:２, beake）*/
	public void order (){
		System.out.print("いらっしゃい、新しい装備が入用かね？　1/2(Y/N) >>");
		int input = scanner.nextInt();
		switch(input){
		case 1:
			smithingWork(getMaterial(), getWeaponName());
			break;
		case 2:
			System.out.println("おや、残念じゃ。また来るがよい。");
			break;
		default:
			System.out.println("なんじゃい、冷やかしか…。");
		}
	}

	/* (非 Javadoc)
	 * @see Smith#smithingWork(java.lang.String, java.lang.String)
	 * 合成用メソッド（予定
	 * [Thread.sleep]使用(try-catch実装済み）*/
	public Object smithingWork(String material, String weaponName){
		try {
			System.out.println("材料と作るものは勝手に決めるべ");
			Thread.sleep(2000);
			System.out.println("コレとコレを…");
			Thread.sleep(1000);
			System.out.println("こうして…");
			Thread.sleep(1000);
			System.out.println("こうして……");
			Thread.sleep(1000);
			System.out.println("こうじゃ！");
			Thread.sleep(3000);
			System.out.println("・・・喜ぶがいい。");
			Thread.sleep(1000);
			System.out.println(material + "製" + weaponName + "が出来たぞい");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		return toString();
	}
	/**コンストラクタ(引数無し)
	 *「weapons」に突っ込む初期設定群
	 *必要であれば以下.addで追加 */
	public Equipment(){
		super ();
	weapons.add("ショートソード");
	weapons.add("ロングソード");
	weapons.add("クレイモア");
	weapons.add("ハンマー");
	weapons.add("アックス");
	weapons.add("ダガー");
	weapons.add("ポールアクス");
	weapons.add("ハルバード");
	weapons.add("グレイブ");
	weapons.add("正体不明な物体");
	}

}
