
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Gui extends JFrame implements ActionListener{

	private JFrame mainFrame;
	private Container contentPane;
	private JTextField textField;
	private JLabel label;
	private Object eventStart;
	private JPanel panel;
	private JLabel outputlabel;


	public void setEventStart (Object eventStarts) {
		this.eventStart = eventStarts;
	}
	public Object getEventStart() {
		return eventStart;
	}
	//============コンストラクタ==============
	Gui (String title){
	//フレーム
	mainFrame = new JFrame(title);
	mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	mainFrame.setSize(450, 250);
	mainFrame.setLocationRelativeTo(null);
	//コンテントパネル
	contentPane = mainFrame.getContentPane();
	//ラベル
	label = new JLabel();
	label.setText("いらっしゃい、新しい装備が入用かね？　1/2(Y/N) >>");
	//テキストフィールド
	textField =new JTextField(3);
	textField.addActionListener(this);
	//パネル
	panel = new JPanel();
	panel.add(label);
	panel.add(textField);
	//出力用のラベル（未実装
	outputlabel = new JLabel();
	outputlabel.setPreferredSize(new Dimension(300,100));
	contentPane.add(panel, BorderLayout.CENTER);
	contentPane.add(outputlabel, BorderLayout.SOUTH);
	mainFrame.setVisible(true);
	}
	//====================================


	//------------------------------
	/* (非 Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 *[textField]に数字を打ち込んだ際の実行文（中途実装
	 */
	public void actionPerformed(ActionEvent event) {
		String input = textField.getText();
		int inputnumber = Integer.parseInt(input);
		switch(inputnumber){
		case 1:
			getEventStart();
			break;
		case 2:
			System.out.println("おや、残念じゃ。また来るがよい。");
			break;
		default:
			System.out.println("なんじゃい、冷やかしか…。");
		}

	}
}